﻿using System;
using System.Collections.Generic;
using System.Text;
using Fluent.Rhx.Syntax;
using Rhino.Geometry;

namespace Fluent.Rhx
{
    public static class CurveExtensions
    {
        public static LocateIdiom Locate(this Curve crv)
        {
            return Given.This(crv).Locate();
        }

        public static VerifyIdiom Verify(this Curve crv)
        {
            return Given.This(crv).Verify();
        }
    }
}
