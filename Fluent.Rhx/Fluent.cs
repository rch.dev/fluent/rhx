﻿using System;
using Rhino.Geometry;
using Fluent.Rhx.Syntax;

namespace Fluent.Rhx
{
    public static class Given
    {
        public static CurveContext This(Curve crv)
        {
            return new CurveContext(crv);
        }
    }
}
