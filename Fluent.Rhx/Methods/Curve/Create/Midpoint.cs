﻿using System;
using System.Collections.Generic;
using System.Text;
using Rhino.Geometry;

namespace Fluent.Rhx.Methods
{
    public static partial class CurveCreateMethods
    {
        public static Point3d Midpoint(Curve crv)
        {
            return crv.PointAtNormalizedLength(0.5);
        }
    }
}
