﻿using System;
using System.Collections.Generic;
using System.Text;
using Rhino.Geometry;

namespace Fluent.Rhx.Methods
{
    public static partial class CurveVerifyMethods
    {
        public static bool IsPlanar(Curve crv)
        {
            crv.TryGetPlane(out var plane);

            return plane != null;
        }
    }
}
