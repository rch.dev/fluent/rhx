﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Rhino.Geometry;
using Act = Fluent.Rhx.Methods.CurveVerifyMethods;

namespace Fluent.Rhx.Syntax
{
    public partial class VerifyIdiom
    {
        private ContextType ContextType { get; set; }
    }

    public partial class VerifyIdiom
    {
        private CurveContext RootCurveContext { get; set; }

        public VerifyIdiom(CurveContext context)
        {
            ContextType = ContextType.CurveContext;
            RootCurveContext = context;
        }

        public CurveContext Then()
        {
            return RootCurveContext;
        }

        public OnlyVerifyCurve Only()
        {
            return new OnlyVerifyCurve(RootCurveContext.InputCurve);
        }

        public static implicit operator bool(VerifyIdiom idiom)
        {
            return idiom.RootCurveContext.VerifyResults.Count > 0 && !idiom.RootCurveContext.VerifyResults.Any(x => x == false);
        }
    }

    public partial class VerifyIdiom
    {
        public VerifyIdiom IsPlanar()
        {
            RootCurveContext.VerifyResults.Add(Act.IsPlanar(RootCurveContext.InputCurve));

            return this;
        }

        public VerifyIdiom IsPlanar(out bool res)
        {
            IsPlanar();

            res = RootCurveContext.VerifyResults.Last();

            return this;
        }
    }

    public class OnlyVerifyCurve
    {
        private Curve ReferenceCurve { get; set; }

        public OnlyVerifyCurve(Curve crv)
        {
            ReferenceCurve = crv;
        }

        public bool IsPlanar()
        {
            return Act.IsPlanar(ReferenceCurve);
        }
    }
}
