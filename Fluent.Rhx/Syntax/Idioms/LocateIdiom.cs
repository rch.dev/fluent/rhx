﻿using System;
using System.Collections.Generic;
using System.Text;
using Rhino.Geometry;

namespace Fluent.Rhx.Syntax
{
    public partial class LocateIdiom
    {
        private ContextType ContextType { get; set; }
    }

    public partial class LocateIdiom
    {
        private CurveContext RootCurveContext { get; set; }

        public LocateIdiom(CurveContext context)
        {
            ContextType = ContextType.CurveContext;
            RootCurveContext = context;
        }

        public CurveContext Then()
        {
            return RootCurveContext;
        }
    }
}
