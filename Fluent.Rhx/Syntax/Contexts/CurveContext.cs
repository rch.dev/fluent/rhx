﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Rhino.Geometry;

namespace Fluent.Rhx.Syntax
{
    public class CurveContext
    {
        public Curve InputCurve { get; set; }
        public List<bool> VerifyResults { get; set; } = new List<bool>();

        public CurveContext(Curve crv)
        {
            InputCurve = crv;
        }

        public void Stop()
        {

        }

        public CurveContextResults Emit()
        {
            return new CurveContextResults(this);
        }

        public LocateIdiom Locate()
        {
            return new LocateIdiom(this);
        }

        public VerifyIdiom Verify()
        {
            return new VerifyIdiom(this);
        }
    }

    public class CurveContextResults
    {
        private CurveContext Context { get; set; }

        public CurveContextResults(CurveContext context)
        {
            Context = context;
        }

        public void VerifyResult(out bool result)
        {
            result = !Context.VerifyResults.Any(x => x == false);
        }

        public void VerifyResults(out List<bool> results)
        {
            results = Context.VerifyResults;
        }
    }
}
