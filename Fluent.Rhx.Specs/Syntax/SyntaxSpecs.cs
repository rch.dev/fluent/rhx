﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using FluentAssertions;
using Fluent.Rhx;
using Rhino.Geometry;
using Fluent.Rhx.Syntax;

namespace Fluent.Rhx.Specs.Syntax
{
    [TestFixture]
    public class SyntaxSpecs
    {
        [Test]
        public void Given_curve_should_allow_context_access_through_base_class()
        {
            var env = new LineCurve(Point3d.Origin, new Point3d(0, 1, 0)).ToNurbsCurve();

            var res = Given.This(env);

            var val = res.GetType() == typeof(CurveContext);

            val.Should().BeTrue("because we are attempting to initialize a new context with a curve.");

            Given.This(env)
                .Verify()
                    .IsPlanar()
                    .Then()
                .Locate()
                    .Then()
                .Emit().VerifyResult(out var result);

            var test = env.Verify().Only().IsPlanar();

            env.Verify()
                .IsPlanar(out var curveIsPlanar)
                .Then()
                .Stop();
        }

        [Test]
        public void Given_curve_should_allow_context_access_through_extension()
        {
            true.Should().BeTrue("tautology");
        }
    }
}
